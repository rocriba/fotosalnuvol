package cat.dam.rocriba.fotosnn;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.FileProvider;

import android.Manifest;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.FirebaseApp;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.UUID;

public class MainActivity extends AppCompatActivity {

    //Inicalize variables
    static final int REQUEST_IMAGE_CAPTURE = 1;
    ImageView foto;
    private String trajImatge;
   private FirebaseStorage firebaseStorage;
    private String idDispositiu;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //Constrain views
        Button btn_ferfoto = findViewById(R.id.btn_captura);
        Button btn_desalocal = findViewById(R.id.desa_local);
        Button btn_desanuvol = findViewById(R.id.desa_nuvol);
        foto = findViewById(R.id.iv_imatge);
        //Inicalize Firebasse app
       FirebaseApp.initializeApp(MainActivity.this);
        firebaseStorage = FirebaseStorage.getInstance();
        getIdDispositiu();

        //For every button call his repspective method
        btn_ferfoto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    TakePictureEvent();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        });

        //For every button call his repspective method
        btn_desalocal.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                galleryAddPic(MainActivity.this);
            }
        });

        //For every button call his repspective method
        btn_desanuvol.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                uploadPic();
            }
        });


    }

    //Method for getting and unic id for variable idDispositiu
    protected synchronized String getIdDispositiu(){
        if (idDispositiu == null){
            SharedPreferences sharedPreferences = this.getSharedPreferences("DEVICE_ID", Context.MODE_PRIVATE);
            idDispositiu = sharedPreferences.getString("DEVICE_ID", null);
            if (idDispositiu == null){
                idDispositiu = UUID.randomUUID().toString();
                SharedPreferences.Editor editor = sharedPreferences.edit();
                editor.putString("DEVICE_ID", idDispositiu);
                editor.commit();

            }

        }
        return idDispositiu;
    }


    //Method for uploading the foto to firebase database
    private void uploadPic(){
        File f = new File(trajImatge);
        Uri uriImatge = Uri.fromFile(f);
        final String cloudFilePath = idDispositiu + uriImatge.getLastPathSegment();
        FirebaseStorage firebaseStorage = FirebaseStorage.getInstance();
        StorageReference storageReference = firebaseStorage.getReference();
        StorageReference uploadref = storageReference.child(cloudFilePath);
        uploadref.putFile(uriImatge).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                Log.e("TAG", "ERROR, no s'ha gaurdat al núvol");
            }
        }).addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
            @Override
            public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                Toast.makeText(MainActivity.this, "La imatge ha estat desada al núvol", Toast.LENGTH_SHORT).show();
            }
        });

    }


    //Method for save the photo to gallery
    private void galleryAddPic(final Context context) {
        Intent mediaScanIntent = new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE);
        File f = new File(trajImatge);
        Uri contentUri = Uri.fromFile(f);
        mediaScanIntent.setData(contentUri);
        this.sendBroadcast(mediaScanIntent);
        ContentValues values = new ContentValues();
        values.put(MediaStore.Images.Media.DATE_TAKEN, System.currentTimeMillis());
        values.put(MediaStore.Images.Media.MIME_TYPE, "image/jpeg");
        values.put(MediaStore.MediaColumns.DATA, trajImatge);
        context.getContentResolver().insert(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, values);

    }

    //Method for set the name of the image
    private File getPictureFile() throws IOException {
        String timeStamp = new SimpleDateFormat("yyyyMMddHHmmss").format(new Date());
        String pictureFile = "IMG_" + timeStamp;

        File storageDir = getExternalFilesDir(Environment.DIRECTORY_PICTURES);
        File image = File.createTempFile(pictureFile, ".jpg", storageDir);
        trajImatge = image.getAbsolutePath();
        return image;
    }

    //Method for take a picture
    private void TakePictureEvent() throws IOException {
        ActivityCompat.requestPermissions(this, new String[]
                { Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE }, 0);
        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        if (takePictureIntent.resolveActivity(getPackageManager()) != null) {

            File pictureFile = getPictureFile();

            if (pictureFile != null){
                Uri photoURI = FileProvider.getUriForFile(this, "cat.dam.rocriba.fotosnn", pictureFile);
                takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, photoURI);
                startActivityForResult(takePictureIntent, REQUEST_IMAGE_CAPTURE);
            }

        }
    }

    //After take the picture, do this
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data){
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode ==REQUEST_IMAGE_CAPTURE && resultCode == RESULT_OK){
            Log.d("Camera demo", "Pic saved");

            File imgFile = new File(trajImatge);
            if(imgFile.exists()) {
                foto.setImageURI(Uri.fromFile(imgFile));
            }

        }
    }
}